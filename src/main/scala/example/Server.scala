package example

import java.util.concurrent.atomic.AtomicLong

import akka.NotUsed
import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem, Props, Terminated}
import akka.http.scaladsl.model.ws.TextMessage
import akka.http.scaladsl.server.{Directives, Route}
import akka.stream.OverflowStrategy
import akka.stream.scaladsl.{Flow, Sink, Source}
import example.models.Book
import play.api.libs.json.{JsValue, Json, Writes}

class Server(system: ActorSystem) extends Directives {
  val route: Route = path("ws") {
    get {
      handleWebSocketMessages(flow)
    }
  }

  def flow: Flow[Any, Nothing, NotUsed] = {
    val id = Connection.getId()
    val connection = system.actorOf(Connection.props(id), Connection.name(id))
    val in = Sink.actorRef(connection, Connection.ConnectionClosed)
    val out = Source.actorRef(8, OverflowStrategy.fail) mapMaterializedValue { a =>
      connection ! Connection.Connected(a)
      a
    }

    Flow.fromSinkAndSource(in, out)
  }
}

class Connection(id: Long) extends Actor with ActorLogging {
  private var connection: Option[ActorRef] = None

  implicit val bookWrites = new Writes[models.Book] {
    override def writes(o: Book): JsValue = Json.obj(
      "title" -> o.title,
      "ibsn" -> o.ibsn,
      "description" -> o.description,
      "thumbnail" -> s"${o.thumbnail}&w=1280",
    )
  }

  override def receive: Receive = {
    case Connection.Connected(a) =>
      connection = Some(a)
      context watch a

    case Terminated(a) if connection contains a =>
      connection = None
      context stop self

    case Connection.ConnectionClosed => context stop self
    case TextMessage.Strict(t) => connection foreach (_ ! TextMessage(s"echo $id $t"))
    case ApiMessages.BookAdded(book) =>
      connection foreach {
        _ ! TextMessage(Json.toJson(book).toString())
      }
    case unsupported: Any => log.warning(s"Unsupported message $unsupported")
  }

  override def postStop(): Unit = connection foreach context.stop
}

object Connection {
  private val id = new AtomicLong()
  private val path = "connection"

  val getId: () => Long = id.incrementAndGet
  def props(id: Long): Props = Props(new Connection(id))
  def name(id: Long) = s"$path$id"
  def nameForAll = s"/user/$path*"

  case class Connected(conn: ActorRef)
  case object ConnectionClosed
}
