package example

import akka.actor.{Actor, Props}
import play.api.libs.json.Json

import scala.concurrent._
import scala.util.{Failure, Success, Try}

class BookRepository extends Actor {

  import context.dispatcher

  override def receive: Receive = {
    case (ApiMessages.AddByIBSN(ibsn), channel: String) =>
      val requestId = sender()

      val bookLoadResult = for {
        book <- Future(loadBookByIBSN(ibsn))
        newBook <- models.BooksDao.create(book)
      } yield {
        requestId ! (ApiMessages.BookAdded(book) -> channel)
        newBook
      }

      bookLoadResult onComplete {
        case Success(bookId) => updateBookToLocalImage(bookId)
        case Failure(e) => requestId ! ApiMessages.BookLoadError(ibsn, e.toString)
      }

    case (ApiMessages.TakeByIBSN(isbn, user), channel: String) =>
      val requestId = sender()
      val updateResult = for {
        b <- models.BooksDao.findByIsbn(isbn)
        book = b.copy(owner = user)
        _ <- models.BooksDao.update(book)
      } yield book

      updateResult onComplete {
        case Success(book) => requestId ! (ApiMessages.BookTaken(book), channel)
        case Failure(e) => requestId ! ApiMessages.BookUpdateError(isbn, e.toString)
      }
  }

  private def loadBookByIBSN(ibsn: String): models.Book = {
    import sys.process._
    val url = s"https://www.googleapis.com/books/v1/volumes?q=isbn:$ibsn"
    val resp = s"curl -k $url".!!
    val json = Json.parse(resp)
    val item = json \ "items" \ 0
    val info = item \ "volumeInfo"
    val title = (info \ "title").as[String]
    val description = (info \ "description").as[String]
    val thumbnail = (info \ "imageLinks" \ "thumbnail").as[String]

    models.Book(None, ibsn, title, None, description, thumbnail)
  }

  private def updateBookToLocalImage(bookId: Long): Unit = {

  }
}

object BookRepository {
  def props = Props[BookRepository]
  def name = "book-repository"
}
