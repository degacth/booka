package example

import akka.actor.{Actor, ActorLogging, ActorSelection, Props}
import slack.rtm.SlackRtmClient
import slack.{SlackUtil, models => sm}

class SlackMessageFrontend(slackClient: SlackRtmClient) extends Actor with ActorLogging {
  private val selfId = slackClient.state.self.id

  import context.system

  override def receive: Receive = {
    case message: sm.Message =>
      val mentionedIds = SlackUtil.extractMentionedIds(message.text)

      if (mentionedIds.contains(selfId)) {
        ApiMessages.extractCommand(message.text) match {
          case ApiMessages.UnsupportedCommand => slackClient.sendMessage(
            message.channel,
            s"What? ${message.text}?"
          )

          case command: ApiMessages.AddByIBSN => bookRepository ! (command -> message.channel)
          case command: ApiMessages.TakeByIBSN =>
            bookRepository ! (command.copy(user = Some(message.user)) -> message.channel)
        }
      }

    case (msg: ApiMessages.BookAdded, channel: String) =>
      context.system.actorSelection(Connection.nameForAll) ! msg

      val b = msg.book
      slackClient.apiClient.postChatMessage(
        channel, "Book added", attachments = Some(sm.Attachment(
          title = Some(b.title),
          image_url = Some(b.thumbnail),
          footer = Some(b.description),
        ) :: Nil)
      )

    case (msg: ApiMessages.BookTaken, channel: String) =>
      val b = msg.book
      val user = s"<@${b.owner.get}>"
      val title = s""""${b.title}""""
      slackClient.apiClient.postChatMessage(
        channel, "Borrowed", attachments = Some(sm.Attachment(
          title = Some(s"Book $title borrowed by $user")
        ) :: Nil)
      )

    case _: sm.Pong =>
    case e: Any => log.info(s"Unsupported message $e")
  }

  def bookRepository: ActorSelection = context.system.actorSelection(s"/user/${BookRepository.name}")
}

object SlackMessageFrontend {
  def props(client: SlackRtmClient) = Props(new SlackMessageFrontend(client))
  def name = "slack"
}
