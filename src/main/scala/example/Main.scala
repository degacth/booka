package example

import akka.actor.{ActorRef, ActorSystem}
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import play.api.libs.json.{JsArray, JsLookupResult, Json}
import slack.rtm.SlackRtmClient

import scala.concurrent.ExecutionContextExecutor
import scala.io.StdIn

object Main extends App {
  val tokenKey = "BOOKA_SLACK_TOKEN"
  val token: Option[String] = sys.env.get(tokenKey)
  implicit val system: ActorSystem = ActorSystem("app")
  implicit val mat: ActorMaterializer = ActorMaterializer()
  implicit val ec: ExecutionContextExecutor = system.dispatcher

  system.actorOf(BookRepository.props, BookRepository.name)

  var slackClient: Option[SlackRtmClient] = None
  var messageFrontend: ActorRef = _

  token foreach { slackToken =>
    slackClient = Some(SlackRtmClient(slackToken))
    slackClient foreach { client =>
      messageFrontend = system.actorOf(SlackMessageFrontend.props(client), SlackMessageFrontend.name)
      client addEventListener messageFrontend
    }
  }

  val (host, port) = (
    sys.env.getOrElse("BOOKA_HOST", "0.0.0.0"),
    sys.env.getOrElse("BOOKA_PORT", "8088"),
  )
  val server = new Server(system)
  val binding = Http().bindAndHandle(server.route, host, port.toInt)

  StdIn.readLine("Press enter to exit")
  slackClient foreach { client =>
    client removeEventListener messageFrontend
    client.close()
  }

  binding flatMap (_.unbind()) onComplete { _ =>
    system.terminate()
  }
}