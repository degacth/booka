package example.models

import slick.jdbc.H2Profile.api._
import slick.sql.{FixedSqlStreamingAction, SqlAction}

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

case class Book(id: Option[Long],
                ibsn: String,
                title: String,
                owner: Option[String],
                description: String,
                thumbnail: String)

class BooksTable(tag: Tag) extends Table[Book](tag, "books") {
  def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
  def ibsn = column[String]("ibsn")
  def title = column[String]("title")
  def owner = column[Option[String]]("owner")
  def description = column[String]("description")
  def thumbnail = column[String]("thumbnail")

  override def * =
    (id.?, ibsn, title, owner, description, thumbnail) <> ((Book.apply _).tupled, Book.unapply)
}

trait DatabaseConfig {
  def db: Database = Database.forConfig("h2mem1")

  implicit val session: Session = db.createSession()
}

trait BaseDao extends DatabaseConfig {
  val booksTable = TableQuery[BooksTable]

  protected implicit def executeFromDb[A](action: SqlAction[A, NoStream, _ <: slick.dbio.Effect]): Future[A] =
    db.run(action)

  protected implicit def executeReadStreamFromDb[A](
                                                     action: FixedSqlStreamingAction[Seq[A], A, _ <: slick.dbio.Effect]
                                                   ): Future[Seq[A]] = db.run(action)
}

object BooksDao extends BaseDao {
  def findAll: Future[Seq[Book]] = booksTable.result
  def findById(id: Long): Future[Book] = booksTable.filter(_.id === id).result.head
  def findByIsbn(ibsn: String): Future[Book] = booksTable.filter(_.ibsn === ibsn).result.head
  def create(book: Book): Future[Long] = booksTable returning booksTable.map(_.id) += book
  def update(book: Book): Future[Int] = booksTable.filter(_.id === book.id)
    .map(_.owner)
    .update(book.owner)

  val setup = DBIO.seq(booksTable.schema.createIfNotExists)
  db.run(setup)
}