package example

object ApiMessages {
  val patternIsbn = "\\d+".r

  def extractCommand(msg: String): SlackMessage = msg
    .replaceAll("^\\s*<.+>\\s*", "") match {

    case msg: String if isISBNCommand(msg, "addByISBN") =>
      patternIsbn.findFirstIn(msg) match {
        case Some(ibsn) => AddByIBSN(ibsn)
        case None => UnsupportedCommand
      }

    case msg: String if isISBNCommand(msg, "getByISBN") =>
      patternIsbn.findFirstIn(msg) match {
        case Some(isbn) => TakeByIBSN(isbn, None)
        case None => UnsupportedCommand
      }

    case _ => UnsupportedCommand
  }

  private def isISBNCommand(msg: String, cmd: String): Boolean = msg.matches(s"^$cmd \\d+")

  sealed trait SlackMessage
  case class AddByIBSN(isbn: String) extends SlackMessage
  case class TakeByIBSN(isbn: String, user: Option[String]) extends SlackMessage
  case object UnsupportedCommand extends SlackMessage

  sealed trait RepositoryMessage
  case class BookAdded(book: models.Book) extends RepositoryMessage
  case class BookTaken(book: models.Book) extends RepositoryMessage
  case class BookLoadError(isbn: String, error: String) extends RepositoryMessage
  case class BookUpdateError(isbn: String, error: String) extends RepositoryMessage
}
