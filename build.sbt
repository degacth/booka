import Dependencies._

ThisBuild / scalaVersion := "2.12.10"
ThisBuild / version := "0.1.0-SNAPSHOT"
ThisBuild / organization := "com.example"
ThisBuild / organizationName := "example"

val akkaVersion = "2.5.25"
val akkaHttpVersion = "10.1.10"
val slickVersion = "3.3.1"

lazy val root = (project in file("."))
  .settings(
    name := "booka",
    libraryDependencies ++= Seq("com.typesafe.akka" %% "akka-actor" % akkaVersion,
      "com.typesafe.akka" %% "akka-stream" % akkaVersion,

      "com.typesafe.akka" %% "akka-http" % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-http-core" % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-http-testkit" % akkaHttpVersion % Test,
      "com.typesafe.play" %% "play-json" % "2.7.3",
      "com.typesafe.slick" %% "slick" % slickVersion,
//      "org.slf4j" % "slf4j-nop" % "1.7.26",
      "com.h2database" % "h2" % "1.4.187",
      "com.typesafe.slick" %% "slick-hikaricp" % slickVersion,
      "org.flywaydb" % "flyway-core" % "3.2.1",

      "com.github.slack-scala-client" %% "slack-scala-client" % "0.2.6",

      scalaTest % Test,
    )
  )

// See https://www.scala-sbt.org/1.x/docs/Using-Sonatype.html for instructions on how to publish to Sonatype.
